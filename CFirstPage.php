<?php
function createAndHideTables() {

    $ViewList=retrieveViewList();
    for($i=0;$i<sizeof($ViewList);$i++) {
        echo "<div class=\"container\" id=\"div_$ViewList[$i]\" style=\"display:none; padding:10% 0 5%;\">";
        echo "<div id=\"wrapper_$ViewList[$i]\" class=\"dataTables_wrapper\" style=\"width:100%\">
                <div class=\"dataTables_length\" id=\"shown_results\">
                    <div class=\"col-9\">";
        echo "<table id=\"$ViewList[$i]\" class=\"display nowrap dataTable dtr-inline collapsed\" cellspacing=\"0\" width=\"100%\" role=\"grid\" aria-describedby=\"example_info\" style=\"width:100%\">";
        $columnNames=getColumnNames($ViewList[$i]);

        createHeaders($columnNames);
        $columnNamesString=getColumnNamesString($columnNames);
        $data=getData($columnNamesString, $ViewList[$i]);
        displaydata($data, $ViewList[$i]);
        echo "</table>";
        
        echo "</div>";
        echo "<button class=\"newregister\"><a href=\"./Form.php?ViewName=$ViewList[$i]\">Nuevo registro</a></button>"; 
        echo "</div>";
        echo "</div>";
        
        echo "</div>";
    }   
    
}
?>

<html>
    <head>
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Billib views</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-theme.css" rel="stylesheet">
        <link rel="stylesheet" href="style/FirstPage.css"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link href="css/jquery.dataTables.min.css" rel="stylesheet">
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.dataTables.min.js"></script>
        <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>

    </head>
    <body>
    <header id="BillibHeader"></header>
    <div class="row">
        <div class="col-3 left">
            <div id="nav">
                <div id="nav2">
                <?php
                include("functions_EM.php");
                $ViewList = retrieveViewList();
                createLeftBar($ViewList);
                ?>
                </div>
            </div>
        </div>
    </div>

    <div id="border"></div>
         
    <?php
        createAndHideTables();
    ?>

    <footer id="BillibFooter">
        <div class="container">
            <center><a href="http://proceedit.blogspot.com.es/">Copyright © 2018 Proceedit, all rights reserved.</a>
        </div>
    </footer>

</body>
        <script>

            $(document).ready(function() {
                $('#ClientesView').DataTable( {
                    columnDefs: [ {
                        targets: [ 0 ],
                        orderData: [ 0, 1 ]
                    }, {
                        targets: [ 1 ],
                        orderData: [ 1, 0 ]
                    }, {
                        targets: [ 4 ],
                        orderData: [ 4, 0 ]
                    } ],
                    "scrollX": true,
                } );
            } );

            $(document).ready(function() {
                $('#OportunidadesView').DataTable( {
                    columnDefs: [ {
                        targets: [ 0 ],
                        orderData: [ 0, 1 ]
                    }, {
                        targets: [ 1 ],
                        orderData: [ 1, 0 ]
                    }, {
                        targets: [ 4 ],
                        orderData: [ 4, 0 ]
                    } ],
                    "scrollX": true,
                    "scrollY": '50vh',
                    "scrollcolapsed": true
                } );
            } );

            $(document).ready(function() {
                $('#AccionesView').DataTable( {
                    columnDefs: [ {
                        targets: [ 0 ],
                        orderData: [ 0, 1 ]
                    }, {
                        targets: [ 1 ],
                        orderData: [ 1, 0 ]
                    }, {
                        targets: [ 4 ],
                        orderData: [ 4, 0 ]
                    } ],
                    "scrollX": true,
                    "scrollY": '50vh',
                    "scrollcolapsed": true
                } );
            } );

            $(document).ready(function() {
                $('#ContactosView').DataTable( {
                    columnDefs: [ {
                        targets: [ 0 ],
                        orderData: [ 0, 1 ]
                    }, {
                        targets: [ 1 ],
                        orderData: [ 1, 0 ]
                    }, {
                        targets: [ 4 ],
                        orderData: [ 4, 0 ]
                    } ],
                    "scrollX": true,
                    "scrollY": '50vh',
                    "scrollcolapsed": true
                } );
            } );


            function displaytable(table, allviews) 
            {
                var views = allviews.split(",");

                for(var i=0;i<views.length;i++) 
                {
                    var x = document.getElementById("div_" + views[i]);
                    x.style.display = "none";
                }
                var y = document.getElementById("div_AccionesView");
                y.style.display = 'block';
            }
        </script>
        <style>
    div.dataTables_wrapper {
        width: 1200px;
        margin: 0 auto;
        align: left;
        text-align: center;
    }
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="materialize/js/materialize.min.js"></script>
    </style>
</html>
