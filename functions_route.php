<?php
require_once('Mysqlconn.php');
require_once("functions_EM_2.php");
if(isset($_GET["left_menu"])){
    echo createLeftBar(retrieveViewList(), $_GET["Rol"]);
}
if(isset($_GET["logout"])){
    session_destroy();
    header('Location: ./Login.php'); 
}
if(isset($_POST["create_tables"])){
    echo createAndHideTables($_POST["Rol"]);
}
if(isset($_POST["fetch_data"])){
    echo getDataTable($_POST["view"], $_POST["Rol"], $_POST["inactive"]);
}
if(isset($_GET["create_tabs_forms"])){
    echo createTabsAndForms($_GET["ViewName"], $_GET["action"], $_GET["id"], $_GET["Rol"]);
}
if(isset($_POST["fetch_select_option"])){
    echo getSelectOptionsArray($_POST["table_name"], $_POST["column_name"], $_POST["inactive"], $_POST["Rol"]);
}

?>