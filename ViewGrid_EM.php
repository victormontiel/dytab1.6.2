<?php
include('functions_EM.php');

if(isset($_GET["ViewName"]))
    {
        $ViewName = $_GET["ViewName"];
    }


 $Name=str_replace("View", "", $ViewName);

 $columnNames=getColumnNames($ViewName);

 $columnNamesString=getColumnNamesString($columnNames);

 $data=getData($columnNamesString, $ViewName);

 function displaybuttons($ViewName) 
 {
 	switch ($ViewName) {
 		default:
 		echo "<a class=\"btn-floating btn-large waves-effect waves-light red\" href=\"./Form.php?ViewName=$ViewName\"><i class=\"material-icons\">+</i></a>";
 		break;
 	}
 }

?>

<html lang="es">
<head>
		
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-theme.css" rel="stylesheet">
	<link href="css/jquery.dataTables.min.css" rel="stylesheet">
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.dataTables.min.js"></script>
	<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>
	<link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>

	<script>
		$(document).ready(function() {
    		$('#MainTable').DataTable( {
        		columnDefs: [ {
            		targets: [ 0 ],
            		orderData: [ 0, 1 ]
        		}, {
            		targets: [ 1 ],
            		orderData: [ 1, 0 ]
        		}, {
            		targets: [ 4 ],
            		orderData: [ 4, 0 ]
        		} ],
        		"scrollX": true,
				"scrollcolapsed": true
    		} );
		} );
	</script>

	<style>
	div.dataTables_wrapper {
        width: 1200px;
        margin: 0 auto;
		align: left;
		text-align: center;
    }
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="materialize/js/materialize.min.js"></script>
	</style>

	</head>
		<body>
			<div class="container">
			<div class="row">
				<?php
				echo "<h2 style=\"text-align:center\">Mantenimiento Arista $Name </h2>";
				?>
			</div>
			<div id="wrapper" class="dataTables_wrapper">
				<div class="dataTables_length" id="shown_results">
				<table id="MainTable" class="display nowrap dataTable dtr-inline collapsed" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width:100%">
					<?php
						createHeaders($columnNames);
						displaydata($data, $ViewName);
					?> 
				</table>
				<?php
				displaybuttons($ViewName);
				?>
			</div>
		</div>
	</body>
	</html>
