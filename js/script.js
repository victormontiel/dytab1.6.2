function prepareJson()
{
	var formData = JSON.stringify($("#mandatoSepa").serializeArray());
	console.log(formData);
}

function divs_Str_fields(object) {
	var button = document.getElementById('download-button');
	button.style.visibility = "hidden";
	document.getElementById('preloader').innerHTML = '<div class="progress"> <div class="indeterminate"></div></div>'
    var divs_Str ="{ ";
    $(object).find('input, textarea, select, button').filter('[type!="hidden"]').each(function () {
        var id = "";
        var value = "";
        var type = "";

        try { id = this.id; } catch (ee) { }
        try {
            value = this.value;
            var attributes = GetAttributeFromField(this);
            if (attributes) value += attributes;
        } catch (ee) { }
        try { type = this.getAttribute("type"); } catch (ee) { }

        //divs_Str += ";;;field===" + id + ";;;value===" + value + ";;;type===" + type;
		divs_Str += "\"" + id + "\":\"" + value + "\"" + ",";		
    });
	
	divs_Str = divs_Str.substring(0, divs_Str.length - 1 ); 
	divs_Str += " }";
	console.log(divs_Str);
	showConfirmButton();
    return divs_Str;
}

function showConfirmButton() {
    var button = document.getElementById('download-button');
    button.style.visibility = "visible";

    var bar = document.getElementById('preloader');
    bar.style.display = "none";
}
  