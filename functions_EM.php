<?php

$clientes_nombre=0;

$clientes_conf=0;

function displayLogInErrorMessage() {
	echo "<div id=\"logInError\" class=\"container\">";
	echo "<div class=\"row\">
          <div class=\"input-field col s12 center\">
            <img src=\"images/BillibLogoTransparentLogIn.png\" alt=\"Billib Logo\" class=\"responsive-img\">
          </div>
        	</div>
  				<div class=\"row\">
    				<div class=\"input-field col s12 center\">
      					<span>Credenciales incorrectas. Por favor, inicie sesión.</span>
    				</div>
    			</div>
    			<div class=\"row\">
    				<a href=\"./LogIn.php\"><button class=\"btn waves-effect waves-light col s4 offset-s4\" name=\"logInPage\">Iniciar sesión<i class=\"material-icons left\">person</i></button></a>
		  		</div>
		  	</div>";
		  	return;

}

function redirectToLogInError($reason) {
	if($reason=="CREDENTIALS") echo "<meta http-equiv=\"refresh\" content=\"0; URL='./LogIn.php?wrong=1'\"/>";
	if($reason=="TIMEOUT") echo "<meta http-equiv=\"refresh\" content=\"0; URL='./LogIn.php?wrong=2'\"/>";
	
}

function getFieldType($table, $column) {
	include ("Mysqlconn.php");

    $query = "SELECT COLUMN_TYPE FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = 'ARista' AND TABLE_NAME = '$table' AND COLUMN_NAME = '$column'";
    $Views_Mysqli = $conexion->query($query);
    $type="";

	while ($row = mysqli_fetch_array($Views_Mysqli, MYSQLI_NUM)) 
	{
		$type=$row[0];		
	}

    return $type;
}

function getAllTypes($View) {

	include ("Mysqlconn.php");


    $query="SELECT COLUMN_TYPE FROM information_schema.COLUMNS where TABLE_SCHEMA='ARista' AND TABLE_NAME = '$View' AND COLUMN_NAME NOT lIKE 'Checksum%' AND COLUMN_NAME NOT LIKE 'Id%' AND COLUMN_NAME NOT LIKE 'Avatar%'";
    //  COLUMN_TYPE FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = 'ARista' AND TABLE_NAME = '$View' AND COLUMN_NAME NOT lIKE 'Checksum%' AND COLUMN_NAME NOT LIKE 'Id%' AND COLUMN_NAME NOT LIKE 'Avatar%'
    $Views_Mysqli = $conexion->query($query);
    $types=array();

	while ($row = mysqli_fetch_array($Views_Mysqli, MYSQLI_NUM)) 
	{
		array_push($types,$row[0]);	
	}


    return $types;
	
}

function retrieveViewList() 
{

	include ("Mysqlconn.php");

    $query = "SHOW FULL TABLES IN ARista WHERE TABLE_TYPE LIKE 'VIEW'";
    $Views_Mysqli = $conexion->query($query);
    $Views = array();

	while ($row = mysqli_fetch_array($Views_Mysqli, MYSQLI_NUM)) 
	{
		if (strpos($row[0], 'Silly') !== false || strpos($row[0], 'Logs') !== false) continue;
		array_push($Views, $row[0]);		
	}

    return $Views;
}

function createLeftBar($array, $Rol) 
{
	if(empty($Rol)) {
		return;
	}
	$rows=sizeof($array);
	$viewArray=$array;
	for($i=0;$i<sizeof($array);$i++) {
		$array[$i]=str_replace("View","", $array[$i]);
	}
	$string=getStringFromArray($viewArray);

	/*echo  "<h2 align=\"center\">Datos ARista</h2>";*/
  	for($i=0;$i<sizeof($array);$i++) 
  	{
  		echo "<ul><li><a class=\"tab-menu\" id=\"tab-$array[$i]\" onclick=\"openTab('tab-$array[$i]')\" href=\"javascript:displaytable('$viewArray[$i]', '$string');setInnerText('BHeader', '$array[$i] - Vista datos');\">$array[$i]</a></li></ul>";
  	}		
}

function getStringFromArray($array) {
	$string="";
	for($i=0;$i<sizeof($array);$i++) {
		$string=$string . "$array[$i]" . ", ";
	}
	$string=substr($string, 0, -2);
	return $string;
}

function createTable($array) 
{
	$rows=sizeof($array);
	$columns=2;

	echo "<table id=\"tabla\">";

	echo   "<tr>
    			<th>Vistas MySql</th>
    			<th>Enlace</th>
  			</tr>";

  	for($i=0;$i<$rows;$i++) 
  	{
  		echo "<tr> <td>";
  		echo $array[$i];
  		echo "</td>";
  		echo "<td><a href=\"./ViewGrid_EM.php?ViewName=$array[$i]\"> <img src=\"https://image.freepik.com/iconos-gratis/plaza-enlace-externo-con-una-flecha-en-diagonal-derecha_318-42130.jpg\" style=\"width:20px;height:20px;\"></td></tr>";
  	}	

  	echo "</table>";	

}

function splitAtUpperCase($string){
    return preg_replace('/(?<!\ )[A-Z]/', ' $0', $string);
}

function checkLongField($field) {
	$LongFields=array("RazonSocial", "DescripcionCampana", "ConceptoFactura", "Descripcion", "NombreCompleto", "Direccion");
	$superLongFields=array("ObservacionesAccion", "RangoCNAECorto");
	if(in_array($field, $LongFields)) return "min-width:200px;";
	if(in_array($field, $superLongFields)) return "min-width:350px;";



	else return "min-width:50px";
}


function createHeaders($columnNames, $Rol, $types, $View) 
{
	echo "<thead>";
	echo "<tr role=\"row\">";

	for($i=-1;$i<sizeof($columnNames);$i++) 
	{
		if(($i==-1 AND strcmp($Rol,"admin")!==0) || ($i==-1 AND $View=="FacturasView") || ($i==-1 AND $View=="GruposView") || ($i==-1 AND $View=="RolesView") || ($i==-1 AND $View=="UsuariosView") || ($i==-1 AND $View=="MembresiasView") ) continue;

		if($i==-1) 
		{
			echo "<th tabindex=\"0\" rowspan=\"1\" colspan=\"1\" align=\"left\" aria-label=\"Actions\" display=\"inline-block;\" type=\"hidden\">Actions</th>";
		}		
		else 
		{
			if(strpos(strtolower($types[$i]),'int') !== false || strpos(strtolower($types[$i]),'long') !== false || strpos(strtolower($types[$i]),'double') !== false || strpos(strtolower($types[$i]),'decimal') !== false) {
				$sum="class=\"sum\"";
			}
			else {
				$sum="";
			}
			$width=checkLongField($columnNames[$i]);
			//$word=splitAtUpperCase($columnNames[$i]);
			echo "<th max-height: 50px; tabindex=\"0\" " . $sum . " style=\"text-align:left;$width\" rowspan=\"1\" colspan=\"1\" aria-label=\"$columnNames[$i]: activate to sort column descending\" display=\"inline-block\" aria-sort=\"ascending\" type=\"hidden\">$columnNames[$i]</th>";
		}
	}
	
	echo "</tr>";
	echo "</thead>";

}

function createFooters($columnNames, $Rol, $types, $View) 
{
	echo "<tfoot>";

	//First row of footers
	echo "<tr>";

	for($i=-1;$i<sizeof($columnNames);$i++) 
	{
		if(($i==-1 AND strcmp($Rol,"admin")!==0) || ($i==-1 AND $View=="FacturasView") || ($i==-1 AND $View=="GruposView") || ($i==-1 AND $View=="MembresiasView") || ($i==-1 AND $View=="UsuariosView") || ($i==-1 AND $View=="RolesView")) continue;
		if($i==-1) echo "<td></td>";
		else {
			//$word=splitAtUpperCase($columnNames[$i]);
		echo "<td class=\"columnSearcher Filter$View\" style=\"display:none;\">Buscar</td>";
		//$columnNames[$i]
		}
		
	}
	echo "</tr>";

	//Second row of footers

	echo "<tr id=\"partials_" . $View . "\">";
	
	for($i=-1;$i<sizeof($types);$i++) 
	{

		if(($i==-1 AND strcmp($Rol,"admin")!==0) || ($i==-1 AND $View=="FacturasView") || ($i==-1 AND $View=="GruposView") || ($i==-1 AND $View=="MembresiasView") || ($i==-1 AND $View=="UsuariosView") || ($i==-1 AND $View=="RolesView")) continue;
		if($i==-1) {
			echo "<td class=\"emptyPartials\"></td>";
			continue;
		}
		if(strpos(strtolower($types[$i]),'int') !== false || strpos(strtolower($types[$i]),'long') !== false || strpos(strtolower($types[$i]),'double') !== false || strpos(strtolower($types[$i]),'decimal') !== false) {
			echo "<td class=\"partials\" style=\"display:none;\"></td>";		
		}
		else {
			echo "<td class=\"emptyPartials\"></td>";	
		}		
	}

	echo "</tr>";

	//Third row of footers
	echo "<tr id=\"totals_" . $View . "\">";
	
	for($i=-1;$i<sizeof($types);$i++) 
	{

		if(($i==-1 AND strcmp($Rol,"admin")!==0) || ($i==-1 AND $View=="FacturasView") || ($i==-1 AND $View=="GruposView") || ($i==-1 AND $View=="MembresiasView") || ($i==-1 AND $View=="UsuariosView") || ($i==-1 AND $View=="RolesView")) continue;
		if($i==-1) {
			echo "<td class=\"emptyTotals\"></td>";
			continue;
		}
		if(strpos(strtolower($types[$i]),'int') !== false || strpos(strtolower($types[$i]),'long') !== false || strpos(strtolower($types[$i]),'double') !== false || strpos(strtolower($types[$i]),'decimal') !== false) {
			echo "<td class=\"totals\" style=\"display:none;\"></td>";		
		}
		else {
			echo "<td class=\"emptyTotals\"></td>";	
		}		
	}

	echo "</tr>";

	//Forth row of footers

	echo "<tr id=\"minimums_" . $View . "\">";
	
	for($i=-1;$i<sizeof($types);$i++) 
	{

		if(($i==-1 AND strcmp($Rol,"admin")!==0) || ($i==-1 AND $View=="FacturasView") || ($i==-1 AND $View=="GruposView") || ($i==-1 AND $View=="MembresiasView") || ($i==-1 AND $View=="UsuariosView") || ($i==-1 AND $View=="RolesView")) continue;
		if($i==-1) {
			echo "<td class=\"emptyMinimum\"></td>";
			continue;
		}
		if(strpos(strtolower($types[$i]),'int') !== false || strpos(strtolower($types[$i]),'long') !== false || strpos(strtolower($types[$i]),'double') !== false || strpos(strtolower($types[$i]),'decimal') !== false) {
			echo "<td class=\"minimum\" style=\"display:none;\"></td>";		
			continue;
		}
		if(strpos(strtolower($types[$i]),'date') !== false) {
			echo "<td class=\"minimum\" style=\"display:none;\"></td>";		
			continue;
		}
		else {
			echo "<td class=\"emptyMinimum\"></td>";	
		}		
	}

	echo "</tr>";

	//Forth row of footers

	echo "<tr id=\"maximums_" . $View . "\">";
	
	for($i=-1;$i<sizeof($types);$i++) 
	{

		if(($i==-1 AND strcmp($Rol,"admin")!==0) || ($i==-1 AND $View=="FacturasView") || ($i==-1 AND $View=="GruposView") || ($i==-1 AND $View=="MembresiasView") || ($i==-1 AND $View=="UsuariosView") || ($i==-1 AND $View=="RolesView")) continue;
		if($i==-1) {
			echo "<td class=\"emptyMaximum\"></td>";
			continue;
		}
		if(strpos(strtolower($types[$i]),'int') !== false || strpos(strtolower($types[$i]),'long') !== false || strpos(strtolower($types[$i]),'double') !== false || strpos(strtolower($types[$i]),'decimal') !== false) {
			echo "<td class=\"maximum\" style=\"display:none;\"></td>";		
			continue;
		}
		if(strpos(strtolower($types[$i]),'date') !== false) {
			echo "<td class=\"maximum\" style=\"display:none;\"></td>";		
			continue;
		}

		else {
			echo "<td class=\"emptyMaximum\"></td>";	
		}		
	}

	echo "</tr>";

	echo "</tfoot>";
}

function getTableFromView($ViewName)
 {
 	if (strpos($ViewName, 'Contac') !== false) {
    return "PersonasContactoClientes";
	}
	if (strpos($ViewName, 'Accion') !== false) {
    return "AccionesSeguimientoOportunidades";
	}
	if (strpos($ViewName, 'ClientesCamp') !== false) {
    return "ClientesCampanasRel";
	}
	if (strpos($ViewName, 'Facturas') !== false) {
    return "Facturas";
	}
	if (strpos($ViewName, 'Grup') !== false) {
    return "GruposBonita";
	}
	if (strpos($ViewName, 'Memb') !== false) {
    return "MembresiasBonitaRel";
	}
	if (strpos($ViewName, 'Role') !== false) {
    return "RolesBonita";
	}
	if (strpos($ViewName, 'Usuar') !== false) {
    return "UsuariosBonita";
	}

	if (strpos($ViewName, 'Clientes') !== false) {
    return "Clientes";
	}
	if (strpos($ViewName, 'Oport') !== false) {
    return "Oportunidades";
	}
	if (strpos($ViewName, 'Campan') !== false) {
    return "Campanas";
	}


 }

function getIdFromTable($table) 
{
	include('Mysqlconn.php'); //Sebastian
	$query="show columns from ARista.$table where 'Key' = 'PRI'";
	$List_Mysqli = $conexion->query($query);

	while ($row = mysqli_fetch_array($List_Mysqli, MYSQLI_NUM)) 
	{
		$table=$row[0];		
	}

	return $table;


}
function check_state($table, $id, $value) 
{
	include ('Mysqlconn.php'); //Sebastian
	$query="select IdEstadoActividad from ARista." . $table . " where $id='" . $value . "'";
	$state_mysqli=$conexion->query($query);

	while ($row = mysqli_fetch_array($state_mysqli, MYSQLI_NUM)) 
	{
		$state=$row[0];	
	}
	return $state;
}

function displaydata($data, $ViewName, $Rol) 
{
	$table=getTableFromView($ViewName);
	$id_name=getPK($table);
	$rows = count($data);
	$columns = max( array_map( 'count',  $data ) );


	echo "<tbody>";
	
	for($i=1;$i<$rows;$i++) 
	{

		if($i%2==1) $oe = "odd";
		else $oe = "even";
		echo "<tr role=\"row\" id=\"$id_name:" . $data[$i][0] . "\" class = \"$oe\">";

		if($data[$i][$columns-1]=='1') $activ=1;
		else $activ=0;

		if(strcmp($Rol,"admin")=='0' && $ViewName!=="FacturasView" && $ViewName!=="GruposView" && $ViewName!=="RolesView" && $ViewName!=="UsuariosView" && $ViewName!=="MembresiasView") {
			if((string) $activ=='0') {
			echo "<td style=\"width:40px;\"> <a href=\"./Form.php?Id=$id_name" . "_" . $data[$i][0] . "&ViewName=$ViewName\"><i class=\"material-icons\">edit</i></a>	
			<a href=\"./DeleteCode.php?Id=$table" . "_" . "$id_name" . "_" . $data[$i][0] . "&ActOrInact=Act\" onclick=\"return confirm('Está seguro de que quiere borrar este registro?')\"> <i class=\"material-icons\">replay</i></a></td>";
		}
		else {
			echo "<td style=\"text-align:center;width:40px;\"> <a href=\"./Form.php?Id=$id_name" . "_" . $data[$i][0] . "&ViewName=$ViewName\"> <i class=\"material-icons\">edit</i> </a>	
			<a href=\"./DeleteCode.php?Id=$table" . "_" . "$id_name" . "_" . $data[$i][0] . "&ActOrInact=Inact\" onclick=\"return confirm('Está seguro de que quiere borrar este registro?')\"> <i class=\"material-icons\">delete</i></a></td>";
		}
		}
			
		
		

		for($j=1;$j<$columns-1;$j++) 
		{	

			echo "<td style=\"text-align:left\" class=\"export\">" . $data[$i][$j] . "</td>";
				
		}
		echo "</tr>";

	}
	echo "</tbody>";



}

function getColumnNames($ViewName) 
{
	include ("Mysqlconn.php");

	$querycolumns_names="SELECT (COLUMN_NAME) FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = 'ARista' AND TABLE_NAME = '$ViewName' AND COLUMN_NAME NOT lIKE 'Checksum%' AND COLUMN_NAME NOT LIKE 'Id%' AND COLUMN_NAME NOT LIKE 'Avatar%';";
    $columnNames_mysqli = $conexion->query($querycolumns_names);

    $columnNames = array();

    //if($ViewName=="OportunidadesView") array_push($columnNames, "IdOportunidad");

	while ($row = mysqli_fetch_array($columnNames_mysqli, MYSQLI_NUM)) 
	{
		if (strpos($row[0], 'Silly') !== false || strpos($row[0], 'Logs') !== false) continue;
		array_push($columnNames, $row[0]);	
	}



	return $columnNames;
}

function filterColumns($columnNames) {
	$rcolumnNames=array();
  for($i=0;$i<count($columnNames);$i++) {
    if(strpos($columnNames[$i], 'Hito')!==false) continue;
    if(strpos($columnNames[$i], 'NumeroOportunidadesAbiertas')!==false) continue;
    if(strpos($columnNames[$i], 'NumeroClientesAsignados')!==false) continue;
    if(strpos($columnNames[$i], 'NumeroRespuestasPositivas')!==false) continue;
    if(strpos($columnNames[$i], 'ResultadoCampana')!==false) continue;
    else array_push($rcolumnNames, $columnNames[$i]);
  }
  return $rcolumnNames;
}

function getColumnNames_table($TableName) 
{
	include ("Mysqlconn.php");

	$querycolumns_names="SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE (TABLE_SCHEMA = 'ARista') AND (TABLE_NAME = '$TableName') AND (COLUMN_KEY <> 'PRI') AND COLUMN_NAME NOT lIKE 'Checksum%' AND COLUMN_NAME NOT like 'IdEstadoActividad%'";

    $columnNames_mysqli = $conexion->query($querycolumns_names);

    $columnNames = array();

	while ($row = mysqli_fetch_array($columnNames_mysqli, MYSQLI_NUM)) 
	{
		if (strpos($row[0], 'Silly') !== false || strpos($row[0], 'Logs') !== false) continue;
		array_push($columnNames, $row[0]);		
	}

	return $columnNames;
}

function getColumnNamesString($columnNames) 
{
	$columnsString="";

	for($i=0;$i<sizeof($columnNames);$i++) 
	{
		$columnsString = $columnsString . $columnNames[$i] . ", ";
	}

	$columnsString = $columnsString . "IdEstadoActividad";

	//$columnsString = substr($columnsString, 0, -2);

	return $columnsString;
}

function getOrder($ViewName) 
{
	switch ($ViewName) 
 	{
    	case "ClientesView":
        	return "CIFNIFNIE";
    	case 1:
        	echo "i equals 1";
        	break;
    	case 2:
        	echo "i equals 2";
        	break;
	}

}

function ID_to_cif($id) {
  include('Mysqlconn.php'); //Sebastian
  $query="Select CIFNIFNIE from ARista.Clientes where IdClienteArista='$id'";
    $col_mysqli=$conexion->query($query);
    $col="";
    while($row = mysqli_fetch_array($col_mysqli, MYSQLI_NUM))
    {
      $col=$row[0];
    }
    return $col;
}

function IdOp_to_cif($id) {
  include('Mysqlconn.php'); //Sebastian
  $query="Select CIFNIFNIE from ARista.Clientes where IdClienteArista=(select IdClienteArista from ARista.Oportunidades where IdOportunidad='$id')";
    $col_mysqli=$conexion->query($query);
    $col="";
    while($row = mysqli_fetch_array($col_mysqli, MYSQLI_NUM))
    {
      $col=$row[0];
    }
    return $col;
}

function getPK($ViewName) 
{
	include('Mysqlconn.php');
	$table=getTableFromView($ViewName);
	$querydata="SHOW KEYS FROM ARista.$table where Key_name = 'PRIMARY'";
	$data_mysqli = $conexion->query($querydata);
	$data=array(array());

	while ($row = mysqli_fetch_array($data_mysqli, MYSQLI_NUM)) 
	{
		array_push($data, $row);		
	}
	$PK=$data[1][4];

	return $PK;
}

function select_multi($query) 
{
	include('Mysqlconn.php');
	$data_mysqli = $conexion->query($query);
	$data=array();
	if(empty($data_mysqli)) return "";

	while ($row = mysqli_fetch_array($data_mysqli, MYSQLI_NUM)) 
	{
		array_push($data, $row);		
	}

	return $data;

}

function getData($columnNamesString, $ViewName)
{
	include ("Mysqlconn.php");
	
	$key=getPK($ViewName);
	$querydata="SELECT $key,".($columnNamesString)." FROM ARista.$ViewName where IdEstadoActividad='1'";
	$data_mysqli = $conexion->query($querydata);
	$data=array(array());

	while ($row = mysqli_fetch_array($data_mysqli, MYSQLI_NUM)) 
	{
		array_push($data, $row);		
	}
	return $data;
}

function newRegisterButton($ViewName) 
{
	echo "<form action=\"InsertForm_EM.php\" method=\"post\">
	<input type=\"hidden\" name=\"action\" value=\"insert_no_action\"/>
	<input type=\"hidden\" name=\"ViewName\" value=\"$ViewName\"/>
	<input type=\"submit\" value=\"Nuevo registro\" class=\"homebutton\" id=\"newRegister\"/>
	</form>";
}

 function check($col_name)
{
	include ("Mysqlconn.php");
	//Select PK Names from SillyTable
	$querytables_names="SELECT TableName, PrimaryKey From ARista.SillyTable where PrimaryKeyValue='$col_name'";
	$row_mysqli = $conexion->query($querytables_names);
	
	$values = array();

	if(count($row_mysqli)==0) return "";

	while($row = mysqli_fetch_array($row_mysqli, MYSQLI_NUM))
		{
			array_push($values, $row[0]);
			array_push($values, $row[1]);
		}
	//$find_value = array();

	return $values;
}

function checkPK($col_name, $ViewName)
{
	include ("Mysqlconn.php"); //Sebastian
	//Select PK Names from SillyTable
	$querytables_names="SELECT TableName, Field From ARista.MaintainanceAux1 where Alias='$col_name'";
	$row_mysqli = $conexion->query($querytables_names);
	$values = array();

	while($row = mysqli_fetch_array($row_mysqli, MYSQLI_NUM))
		{
			array_push($values, $row[0]);
			array_push($values, $row[1]);
		}

	$querycheck="SELECT ReferencedTableName, ComboValue from ARista.ForeignKeysAux where ReferencedTableName='" . $values[0] . "' and ComboValue='" . $values[1] . "'";	
	$finalvalues_mysqli=$conexion->query($querycheck);
	$finalvalues=array();


	while($row = mysqli_fetch_array($finalvalues_mysqli, MYSQLI_NUM))
		{
			array_push($finalvalues, $row[0]);
			array_push($finalvalues, $row[1]);
		}

	if(count($finalvalues)==0) return "";


	//CIFNIFNIE CLIENTES
	if($finalvalues[0]=='Clientes' && $finalvalues[1]=='CIFNIFNIE') return "";
	return $values;
}

function checkPK_table($col_name, $TableName)
{
	include ("Mysqlconn.php"); //Sebastian
	$querycheck="SELECT ReferencedTableName, ComboValue from ARista.ForeignKeysAux where TableName='" . $TableName . "' and ColumnName='" . $col_name . "'";	
	$finalvalues_mysqli=$conexion->query($querycheck);
	$finalvalues=array();

	while($row = mysqli_fetch_array($finalvalues_mysqli, MYSQLI_NUM))
		{
			array_push($finalvalues, $row[0]);
			array_push($finalvalues, $row[1]);
		}

	if(count($finalvalues)==0) return "";


	//CIFNIFNIE CLIENTES
	return $finalvalues;
}


function createSimpleHeaders($columnNames) 
{
	echo "<thead>";
	echo "<tr role=\"row\">";

	for($i=0;$i<sizeof($columnNames);$i++) 
	{
		echo "<th tabindex=\"0\" rowspan=\"1\" colspan=\"1\" align=\"center\" display=\"inline-block\">$columnNames[$i]</th>";		
	}
	echo "</thead>";
	echo "</tr>";
}
function convert_to_id($field) 
{
	include ("Mysqlconn.php");
	$query = "SELECT ColumnName FROM ARista.ForeignKeysAux where ComboValue='$field'";
	$row_mysqli=$conexion->query($query);


	while ($row = mysqli_fetch_array($row_mysqli, MYSQLI_NUM)) 
	{
		$id_value=$row[0];		
	}

	if(empty($id_value)) return $field;

	return $id_value;
}

function createSelect($values, $col_name, $table) 
{
	include ("Mysqlconn.php");
	$query = "SELECT " . $values[1] . " FROM ARista." . $values[0] . " order by " . $values[1];

	$row_mysqli=$conexion->query($query);
	$select_array = array();

	while ($row = mysqli_fetch_array($row_mysqli, MYSQLI_NUM)) 
	{
		array_push($select_array, $row[0]);		
	}
	if(strpos($col_name, 'Id')!==false) $col_name2 = substr($col_name, 2);
	else $col_name2=$col_name;

	$mand=check_mandatory($col_name, $table);
	if($mand==1) {
		$star='*';
		$required="class=\"required\" oninvalid=\"this.setCustomValidity('Este campo es obligatorio')\" oninput=\"setCustomValidity('')\"";
		$requiredDiv="name=\"requiredSelectDiv\"";
	}
	else {
		$star='';
		$required="";
		$requiredDiv="";
	}

	echo "<div class=\"input-field col s12\"" . $requiredDiv . ">
			<select id=\"$table" . "_$col_name\" style=\"width: 400px\" name=\"" . addslashes($col_name) . "\" " . $required . ">
			//$values[1]
				<option value=\"\" disabled selected>" . $col_name2 . "</option>";
	for($i=0;$i<count($select_array);$i++) 
	{
		echo "<option>$select_array[$i]</option>";
	}
	echo "</select>";
			echo "<label>" . $col_name2 . "$star</label>";
	echo "</div>";
	echo  "<br>";
	echo  "<br>";

	initialize_select("$table" . "_$col_name");
	

}

/*function createInsertForm($columnNames, $TableName) 
{
	echo "<fieldset>
    			<form class=\"col s12\" method=\"POST\" action=\"InsertCode_EM.php\">
    				<b align=\"left\">Añadir nuevos datos</b>
    				<br><br>";
    echo "<input type =\"hidden\" name=\"TableName\" value=\"$TableName\"/>";
	
	for($i=0;$i<count($columnNames);$i++) 
	{
		$values=CheckPK_table($columnNames[$i], $TableName);
		if(empty($values)) 
		{
			createSimpleField($columnNames[$i]);
		}
		else 
		{
			createSelect($values);
		}

	}
	echo "<input type=\"submit\" value=\"Añadir registro\" id=\"Insert\"/>";
	echo "</form>
			</fieldset>";
}*/

function FieldTypetoType($fieldType) {


	if(strpos(strtolower($fieldType), "int")!==false || strpos(strtolower($fieldType), "decimal")!==false || strpos(strtolower($fieldType), "double")!==false) return "\"text\" onkeyup=\"digitsOnly(this);\"";

	return "text";
}

function sessionTimeout()
{
	$timeout = 1800; // Number of seconds until it times out.
 
// Check if the timeout field exists.
if(isset($_SESSION['timeout'])) {
    // See if the number of seconds since the last
    // visit is larger than the timeout period.
    $duration = time() - (int)$_SESSION['timeout'];
    if($duration > $timeout) {
        // Destroy the session and restart it.
        session_destroy();
        session_start();
        return "TIMEOUT";
    }
}
 
// Update the timeout field with the current time.
$_SESSION['timeout'] = time();
return "";

}



function createSimpleField($FieldName, $table) 
{
	
	$mand=check_mandatory($FieldName, $table);
	$fieldType=getFieldType($table, $FieldName);
/*	echo "<script type='text/javascript'>alert('$table');</script>";
	echo "<script type='text/javascript'>alert('$FieldName');</script>";
	echo "<script type='text/javascript'>alert('$fieldType');</script>";*/
	$type=FieldTypeToType($fieldType);
	//echo "<script type='text/javascript'>alert('$type');</script>";
	if($mand==1) $star='*';
	else $star='';
	if (strpos($FieldName, 'Fecha') !== false) 
	{
		echo "<div class=\"row\">
        		<div class=\"input-field col s6\">";
        		if($mand==1) {
        			echo "<input id=\"$FieldName\" type=\"text\" required=\"\" aria-required=\"true\" oninvalid=\"this.setCustomValidity('Este campo es obligatorio')\" oninput=\"setCustomValidity('')\" class=\"datepicker\" name=\"$FieldName\"/>";
        		}
        		else {
        			echo "<input id=\"$FieldName\" type=\"text\" class=\"datepicker\" name=\"$FieldName\"/>";
        		}
          				
          			echo "<label for=\"$FieldName\">$FieldName$star</label>     				
        		</div>
      		 </div>";
	}

	else 
	{	
      	echo "<div class=\"row\">
        		<div class=\"input-field col s6\">";
        		if($mand==1) {
        			echo "<input id=\"$FieldName\" type=" . $type . " required=\"\" aria-required=\"true\" oninvalid=\"this.setCustomValidity('Este campo es obligatorio')\" oninput=\"setCustomValidity('')\" class=\"validate\" name=\"$FieldName\"/>";
        		}
        		else {
        			echo "<input id=\"$FieldName\" type=" . $type . " class=\"validate\" name=\"$FieldName\"/>";
        		}
          				
          		echo "<label for=\"$FieldName\">$FieldName$star</label>
        		</div>
      		 </div> 
      		 ";
	}
	
}

function initialize_select($id) 
{
	echo "<script type=\"text/javascript\"> 
		$(document).ready(function() {
    		$('#$id').material_select();
  		});
  		</script>";
}

function getViewFromTable($table) 
{
	switch ($table) 
	{
		case 'Clientes':
		return "ClientesView";
		break;

		case 'ClientesDatosOperativosDet':
		return "ClientesView";
		break;

		case 'ClientesDatosEconomicosDet':
		return "ClientesView";
		break;

		case 'ClientesConfiguracionesOperativasDet':
		return "ClientesView";
		break;

		case 'Facturas':
		return "FacturasView";
		break;

		case 'FacturasEvolucionDet':
		return "FacturasView";
		break;

		case 'AccionesSeguimientoOportunidades':
		return "AccionesView";
		break;

		case 'Oportunidades':
		return "OportunidadesView";
		break;

		case 'Campanas':
		return "CampanasView";
		break;

		case 'RolesBonita':
		return "RolesView";
		break;


	}


}


?>