<?php
session_start();

include('functions_EM.php');
$timeout=sessionTimeout();
//Retrieve the parameters from the LogIn page and decide if it is correct or not.

    $email="";
    $password="";
    $Rol="";

  if(isset($_POST["email"]) && isset($_POST["password"]))
    {
        $email = $_POST["email"];
        $password = $_POST["password"];
        $_SESSION["Rol"]=getRol($email, $password);
        $timeout="";
    }

    //if(isset($_POST["rol"])) $_SESSION["Rol"]=$_POST['rol'];


    if(isset($_SESSION["Rol"])) $Rol=$_SESSION["Rol"];

    if(!empty($timeout)) {
        redirectToLogInError("TIMEOUT");
    }

    if(empty($Rol)){
        redirectToLogInError("CREDENTIALS");
    }

    function TryLogIn($email, $password) {
        $r = new HttpRequest('http://35.156.33.57:8080/bonita/', HttpRequest::METH_POST);
        $r->addPostFields(array('email' => '$email', 'password' => '$password', 'redirect' => 'false', 'redirectUrl' => ''));
        try
        {
            echo $r->send()->getBody();
        }
        catch (HttpException $ex) 
        {
            echo $ex;
        }
    }

    function getRol($email, $password) {
        $hashPass=hash("sha256", $password);

            include ("Mysqlconn.php");

            $query="SELECT RolARista FROM ARista.UsuariosBonita WHERE CorreoElectronicoEmpresa='$email'";

            $RolARista_mysqli = $conexion->query($query);

            $RolARista="";

            while ($row = mysqli_fetch_array($RolARista_mysqli, MYSQLI_NUM)) 
            {
            if(empty($row[0])) return "";
            $RolARista=$row[0];      
            }

            $query="SELECT Password FROM ARista.RolesArista WHERE RolArista='$RolARista'";

            $password_mysqli = $conexion->query($query);

            while ($row = mysqli_fetch_array($password_mysqli, MYSQLI_NUM)) 
            {
            $hashPassARista=$row[0];      
            }

            if(strcmp($hashPass, $hashPassARista)=='0') return $RolARista;
            else return "";    
    }

function createAndHideTables($Rol) {

    if(empty($Rol)) {
        //displayLogInErrorMessage();
        redirectToLogIn();
        return;
    }

    $ViewList=retrieveViewList();

    for($i=0;$i<sizeof($ViewList);$i++) {
    	echo "<div id=\"div_$ViewList[$i]\" style=\"display:none; position:absolute; top: 4%; left: 4%; width:100%; table-layout:fixed\">";

    	echo "<table id=\"$ViewList[$i]\" class=\"display\" cellspacing=\"0\" width=\"100%\">";        
        $columnNames=getColumnNames($ViewList[$i]);
        $types=getAllTypes($ViewList[$i]);
        createHeaders($columnNames, $Rol, $types, $ViewList[$i]);
        $columnNamesString=getColumnNamesString($columnNames);
        $data=getData($columnNamesString, $ViewList[$i]);
        displaydata($data, $ViewList[$i], $Rol);
        createFooters($columnNames, $Rol, $types, $ViewList[$i]);
        /*if(strcmp($Rol,"admin")==0) {
            if(strpos($ViewList[$i], 'Oportunidades') === false && strpos($ViewList[$i], 'Acciones') === false && strpos($ViewList[$i], 'Grupos') === false && strpos($ViewList[$i], 'Membresias') === false && strpos($ViewList[$i], 'Usuarios') === false && strpos($ViewList[$i], 'Roles') === false) {
                echo "<a href=\"./Form.php?ViewName=$ViewList[$i]\" style=\"position: relative; left: 56vw; top: 36px; margin-left:15px; z-index: 1;\"><button class=\"btn pulse\">Nuevo Registro<i class=\"material-icons right\">add_box</i></button></a>";
            }
            else {
                echo "<a style=\"position: relative; left: 54vw; top: 36px; margin-left:15px; z-index: 1;\"><button class=\"btn\" disabled>Nuevo Registro<i class=\"material-icons right\">add_box</i></button></a>";
            }
            
        }*/
		
        echo "</table>";
		//***Button Sample
		//<button class=\"btn waves-effect waves-light pulse\" type=\"submit\" name=\"action\">Nuevo Registro</button>
		//***Previous button nuevo registro
        //echo "<button class=\"btn waves-effect waves-light\" type=\"submit\" name=\"action\">Añadir registro<i class=\"material-icons right\">cloud</i></button>";
		echo "</div>";
    }   
}

function disableButtons($Rol) {
    if(strpos(strtolower($Rol),'admin') !== false) return;
    echo "var buttons=$('.dt-button btn pulse');
            for(var i=0;i<buttons.length;i++) {
                buttons[i].disable();
            }";
}

function insertButton($Rol, $View) {
    if(strpos(strtolower($Rol),'admin') !== false){
        echo ",
                                //NEW BUTTON NUEVO REGISTRO
                                {
                                  text: 'Nuevo Registro<i class=\"material-icons right\" >add_box</i>',
                                  action: function ( e, dt, button, config ) {
                                    window.location = 'Form.php?ViewName=$View';
                                  },
                                  \"className\":'btn pulse'        
                                }";
    }
}

    


function initialiseRanges1($view, $columnNames, $types, $rol) {
    $sum=0;
    if($rol=='admin' && $view!=="FacturasView"  && $view!=="GruposView" && $view!=="RolesView" && $view!=="UsuariosView" && $view!=="MembresiasView") $sum=1;
    $table=str_replace("View","", $view);
    for($i=0;$i<sizeof($types);$i++) {
        $j=$i+$sum;
        if(strpos(strtolower($types[$i]),'int') !== false || strpos(strtolower($types[$i]),'long') !== false || strpos(strtolower($types[$i]),'double') !== false || strpos(strtolower($types[$i]),'decimal') !== false) {
            echo "$.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                if ( settings.nTable.id != '$view')
                {
                return true;
                }
                var min = (parseFloat($('tr:eq(3) td:eq($j) input', table" . $table . ".table().footer()).val()) || (-1)*Number.MAX_SAFE_INTEGER);
                var max = (parseFloat($('tr:eq(4) td:eq($j) input', table" . $table . ".table().footer()).val()) || Number.MAX_SAFE_INTEGER);
                var age = parseFloat( data[$j] ) || 0; // use data for the age column
 
                if ( ( isNaN( min ) && isNaN( max ) ) ||
                    ( isNaN( min ) && age <= max ) ||
                    ( min <= age   && isNaN( max ) ) ||
                    ( min <= age   && age <= max ) ) return true;
            
                return false;
                });";
                continue;
     
        }

        if(strpos(strtolower($types[$i]), 'date') !== false) {
            echo "$.fn.dataTableExt.afnFiltering.push(
            function( oSettings, data, iDataIndex ) {
                if ( oSettings.nTable.id != '$view')
                {
                return true;
                }
                var dataDate = data[$j];
                if(!data[$j]) return true;
                var iFini = ($('tr:eq(3) td:eq($j) input', table" . $table . ".table().footer()).val() || '1800-01-01');
                var iFfin = ($('tr:eq(4) td:eq($j) input', table" . $table . ".table().footer()).val() || '3000-12-12');
                

                var finalDate=iFfin.split('-').join('');
                var startDate=iFini.split('-').join('');
                var Date=dataDate.split('-').join('');


 
                if((parseInt(finalDate) - parseInt(Date))<0) return false;
                if((parseInt(Date) - parseInt(startDate))<0) return false;
                return true;
                }
            );";
     
        }



    }
}

    function initialiseRanges2($view, $columnNames, $types, $rol) {
        $sum=0;
    if($rol=='admin' && $view!=="FacturasView"  && $view!=="GruposView" && $view!=="RolesView" && $view!=="UsuariosView" && $view!=="MembresiasView") $sum=1;
        $table=str_replace("View","", $view);
        for($i=0;$i<sizeof($types);$i++) {
            $j=$i+$sum;
        if(strpos(strtolower($types[$i]),'int') !== false || strpos(strtolower($types[$i]),'long') !== false || strpos(strtolower($types[$i]),'double') !== false || strpos(strtolower($types[$i]),'decimal') !== false || strpos(strtolower($types[$i]),'date') !== false) {
        /*echo "$('tr:eq(3) td:eq($j) input', table" . $table . ".table().footer()).keyup( function(event) {
            if(event.keyCode === 13) table" . $table . ".draw();
            });\n";
        echo "$('tr:eq(4) td:eq($j) input', table" . $table . ".table().footer()).keyup( function(event) {
            if(event.keyCode === 13) table" . $table . ".draw();
            });\n";*/

        echo "$('tr:eq(3) td:eq($j) input', table" . $table . ".table().footer()).change( function(event) {
            table" . $table . ".draw();
            });\n";
        echo "$('tr:eq(4) td:eq($j) input', table" . $table . ".table().footer()).change( function(event) {
            table" . $table . ".draw();
            });\n";
    }
}    

    
}


?>


<html>
    <head>
        <title>BilliB ARista</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- GOOGLE FONTS + MATERIAL ICONS -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- FAVICON -->
        <link rel="shortcut icon" type="image/png" href="https://billibfinance.com/wp-content/uploads/2017/10/favicom.png"/>
        <!-- FRAMEWORKS -->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>         
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-theme.css" rel="stylesheet">
        <link href="css/jquery.dataTables.min.css" rel="stylesheet">
        <!-- CSS -->
        <link rel="stylesheet" href="style/FirstPage.css"/>
        <!-- FRAMEWORK SCRIPTS -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="//cdn.datatables.net/plug-ins/1.10.16/api/fnFilterOnReturn.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
        <script src="https://cdn.datatables.net/select/1.2.5/js/dataTables.select.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
        <script src="https://cdn.datatables.net/plug-ins/1.10.16/api/sum().js"></script>
        <script type="text/javascript" src="js/firstpage.js"></script>    
    </head>
    <body>
        <header id="BillibHeader"><h2 id="BHeader" align=\"center\">ARIsta_web. Aplicación de consulta y mantenimiento</h2></header>
        <!-- PRELOADER -->
        <div class="preloader-background">
            <h3 class="preloader-msg">Espere por favor, la aplicación está cargando</h3>
            <div class="preloader-wrapper big active">
                <div class="spinner-layer spinner-blue-only">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- CONTAINER -->
        <div class="container-fluid main-container">
            <div class="row">
                <!-- LEFT MENU -->
                <div class="col-sm-1 col-md-1 col-lg-1 menu">
                    <div class="menu-views">
                        <?php
                        $ViewList = retrieveViewList();
                        createLeftBar($ViewList, $Rol);
                        ?>                      
                    </div>
                </div>
                <!-- TABLES -->
                <div id="table_container" class="col-sm-11 col-md-11 col-lg-11 tables-container">
                    <?php
                    createAndHideTables($Rol);
                    ?>
                </div>
                <!--<div id="border"></div>-->                  
            </div>
        </div>  
        <footer id="BillibFooter">
            <p style="position: relative; left: 39vw; bottom: 0px; font-weight: 600; font-size: 13px; color: #3a3a3a;">Powered by dyTAB</p>
            <div class="container">
                <center><a href="http://proceedit.blogspot.com.es/" style="color:black; font-size: 13px; font-weight: 600;">Copyright © 2018 Proceedit, all rights reserved.</a>
            </div>
        </footer>       
    </body>
    <!-- OUR SCRIPT! -->
    <script type="text/javascript" language="javascript" class="init">
 
    /*MENU TAB DISPLAYING BUTTON*/
    function openTab(tab_name){ 
        var alltabs = document.getElementsByClassName("tab-menu");
        var tab = document.getElementById(tab_name);

   for(i = 0; i <  alltabs.length; i++) {
            alltabs[i].style.backgroundColor = "";
            alltabs[i].style.color = "";
        }

        tab.style.color = "#00ffd1";
        tab.style.backgroundColor = "#15202b";
        tab.style.borderRadius = "3px";
        tab.style.width = "auto";
    } 

       
        /*$(document).ready(function () {             
            $('.dataTables_filterinput[type="search"]').css(
                {'width':'10px','display':'inline-block'}
                );
        });*/

        $(document).ready(function() {
            $('.datepicker').pickadate(
                { 
                selectMonths: true,
                 selectYears: 15, 
                 today: 'Today',
                 format: 'yyyy-mm-dd', 
                 clear: 'Clear',
                 close: 'Ok',
                 closeOnSelect: false,
                 formatSubmit: 'yyyy-mm-dd'
                });



            });


    $(document).ready(function() {

        //Initialises all columnSearchers
                
        $('.ColumnSearcher').each( function () {
            var title = "Buscar";
            $(this).html( '<input type="text" placeholder="'+title+'" />' );
            });

        $('.minimum').each( function () {
            var title = "Min";
            $(this).html( '<input type="text" placeholder="'+title+'" />' );
            });

        $('.maximum').each( function () {
            var title = "Max";
            $(this).html( '<input type="text" placeholder="'+title+'" />' );
            });

         


        //CLIENTES

        var startTable="ClientesView";
       

        <?php
        $name="ClientesView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges1($name, $columnNames, $types, $Rol);
        ?>

       /* $('.btn-filtros').click( function (){
            alert("Kaname Click");
        });*/

        var tableClientes = $('#' + startTable).DataTable({
                    scrollX:true,
                    autoWidth : true,
                    "language": {
                        "sUrl": "Language/Spanish.json"
                    },
                    dom: 'Blfrtip',
                    lengthMenu: [
                    [ 25, 50, -1 ],
                    [ '25', '50', 'Todos' ]
                    ],
                    buttons: [
                                {
                                    
                                    text: 'Filtros',
                                    action: function (e, dt, node, config) {     
                                        var rows = document.getElementsByClassName('columnSearcher');
                                        var hide;
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            hide=0;
                                            }
                                            else {
                                                rows[i].style.display='none';
                                                hide=1;   
                                            }
                                        }                                     
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                columnSearcherCleaner("ClientesView");
                                            }
                                        else 
                                        {
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw();
                                    },
                                    "className":'btn btn-filtros'       
                                },
                                {
                                    text: 'Rangos',
                                    action: function (e, dt, node, config) {
                                        var mins = document.getElementsByClassName('minimum');
                                        var maxs = document.getElementsByClassName('maximum');
                                        var hide;
                                        for(var i=0;i<mins.length;i++) {
                                            if(mins[i].style.display =='none') {
                                            mins[i].setAttribute('style', '');
                                            maxs[i].setAttribute('style', '');
                                            hide=0;
                                            
                                        }
                                        else {
                                            mins[i].style.display='none';
                                            maxs[i].style.display='none';
                                            hide=1;
                                        }
                                        }

                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                minCleaner();
                                                maxCleaner();                                           
                                        }
                                        else 
                                        {
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-rangos'
                                },
                                {
                                    text: 'Parciales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('partials');
                                        var raws = document.getElementsByClassName('emptyPartials');
                                        var hide;
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide=0;
                                            
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide=1;
                                        }
                                        }
                                       /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-parciales'
                                },
                                {
                                    text: 'Totales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('totals');
                                        var raws = document.getElementsByClassName('emptyTotals');
                                        var hide;
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide=0;
                                            
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide=1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-totales'
                                },
                                {
                                    extend: 'selectAll',
                                    text:'Todos<i class="material-icons right">done_all</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'selectNone',
                                    text:'Ninguno<i class="material-icons right">remove_circle_outline</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'csv',
                                    text:'CSV<i class="material-icons right">file_download</i>',
                                    exportOptions: {
                                    columns: ':visible:not(:eq(0))',
                                    rows:'.selected',
                                },
                                "className":'btn'
                                },
                                {
                                extend: 'excel',
                                text:'EXCEL<i class="material-icons right">file_download</i>',
                                exportOptions: {
                                rows:'.selected',
                                columns: ':visible:not(:eq(0))',
                                },
                                "className":'btn'
                                }
                                <?php
                                insertButton($Rol,"ClientesView");
                                ?>
       //'colvis'         
                            ],
                    initComplete: function () {
                        tableClientes.columns().every( function () {
                        var that = this;

                        //Searchers 
                        $( 'input', this.footer() ).change( function(event) {
                        if ( that.search() !== this.value ) {
                                that
                                .search( this.value )
                                .draw();

                    }                         
                    } );
                    });
                    },

                    footerCallback: function (row, data, start, end, display) {
                        var api=this.api(), data;

                        api.columns('.sum', {page: 'current'}).every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                        return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(1) td:eq(' + index + ')', tableClientes.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                        api.columns('.sum').every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                            return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(2) td:eq(' + index + ')', tableClientes.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                    }
                    });

    $('#' + startTable + ' tbody').on( 'click', 'tr', function () {
      $(this).toggleClass('selected');
    } );

    <?php
    $name="ClientesView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges2($name, $columnNames, $types, $Rol);
    ?>


    //ACCIONES

    <?php
    $name="AccionesView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges1($name, $columnNames, $types, $Rol);
    ?>

    startTable="AccionesView";

        var tableAcciones = $('#' + startTable).DataTable({
                    scrollX:true,
                    autoWidth : true,
                    "language": {
                        "sUrl": "Language/Spanish.json"
                    },
                    dom: 'Blfrtip',
                    lengthMenu: [
                    [ 25, 50, -1 ],
                    [ '25', '50', 'Todos' ]
                    ],
                    buttons: [
                                {
                                    text: 'Filtros',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('columnSearcher');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            hide = 0;                                           
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                columnSearcherCleaner("AccionesView");
                                        }
                                        else 
                                        {
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw();
                                    },
                                    "className":'btn btn-filtros'
                                },
                                {
                                    text: 'Rangos',
                                    action: function (e, dt, node, config) {
                                        var mins = document.getElementsByClassName('minimum');
                                        var maxs = document.getElementsByClassName('maximum');
                                        for(var i=0;i<mins.length;i++) {
                                            if(mins[i].style.display =='none') {
                                            mins[i].setAttribute('style', '');
                                            maxs[i].setAttribute('style', '');
                                            hide = 0;
                                            
                                        }
                                        else {
                                            mins[i].style.display='none';
                                            maxs[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                minCleaner();
                                                maxCleaner();
                                        }
                                        else 
                                        {
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-rangos'
                                },
                                {
                                    text: 'Parciales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('partials');
                                        var raws = document.getElementsByClassName('emptyPartials');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-parciales'
                                },
                                {
                                    text: 'Totales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('totals');
                                        var raws = document.getElementsByClassName('emptyTotals');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-totales'
                                },

                                {
                                    extend: 'selectAll',
                                    text:'Todos<i class="material-icons right">done_all</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'selectNone',
                                    text:'Ninguno<i class="material-icons right">remove_circle_outline</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'csv',
                                    text:'CSV<i class="material-icons right">file_download</i>',
                                    exportOptions: {
                                    columns: ':visible:not(:eq(0))',
                                    rows:'.selected'
                                },
                                "className":'btn'
                                },
                                {
                                extend: 'excel',
                                text:'EXCEL<i class="material-icons right">file_download</i>',
                                exportOptions: {
                                rows:'.selected',
                                columns: ':visible:not(:eq(0))'
                                },
                                "className":'btn'
                                }
                                <?php
                                insertButton($Rol,"AccionesView");
                                ?>      
                            ],
                    initComplete: function () {
                        tableAcciones.columns().every( function () {
                        var that = this;

                        //Searchers 
                        $( 'input', this.footer() ).change( function(event) {
                        if ( that.search() !== this.value ) {
                                that
                                .search( this.value )
                                .draw();
                    }                         
                    } );
                    });
                    },

                    footerCallback: function (row, data, start, end, display) {
                        var api=this.api(), data;

                        api.columns('.sum', {page: 'current'}).every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                        return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(1) td:eq(' + index + ')', tableAcciones.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                        api.columns('.sum').every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                            return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(2) td:eq(' + index + ')', tableAcciones.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                    }
                    });

    $('#' + startTable + ' tbody').on( 'click', 'tr', function () {
      $(this).toggleClass('selected');
    } );

    <?php
    $name="AccionesView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges2($name, $columnNames, $types, $Rol);
    ?>



        startTable="CampanasView";

    <?php
    $name="CampanasView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges1($name, $columnNames, $types, $Rol);
    ?>

        var tableCampanas = $('#' + startTable).DataTable({
                    scrollX:true,
                    autoWidth : true,
                    "language": {
                        "sUrl": "Language/Spanish.json"
                    },
                    dom: 'Blfrtip',
                    lengthMenu: [
                    [ 25, 50, -1 ],
                    [ '25', '50', 'Todos' ]
                    ],
                    buttons: [
                                {
                                    text: 'Filtros',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('columnSearcher');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            hide = 0;                                           
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                columnSearcherCleaner("CampanasView");
                                        }
                                        else 
                                        {
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-filtros'
                                },
                                {
                                    text: 'Rangos',
                                    action: function (e, dt, node, config) {
                                        var mins = document.getElementsByClassName('minimum');
                                        var maxs = document.getElementsByClassName('maximum');
                                        for(var i=0;i<mins.length;i++) {
                                            if(mins[i].style.display =='none') {
                                            mins[i].setAttribute('style', '');
                                            maxs[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            mins[i].style.display='none';
                                            maxs[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                minCleaner();
                                                maxCleaner();
                                        }
                                        else 
                                        {
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-rangos'
                                },
                                {
                                    text: 'Parciales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('partials');
                                        var raws = document.getElementsByClassName('emptyPartials');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-parciales'
                                },
                                {
                                    text: 'Totales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('totals');
                                        var raws = document.getElementsByClassName('emptyTotals');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-totales'

                                },

                                {
                                    extend: 'selectAll',
                                    text:'Todos<i class="material-icons right">done_all</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'selectNone',
                                    text:'Ninguno<i class="material-icons right">remove_circle_outline</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'csv',
                                    text:'CSV<i class="material-icons right">file_download</i>',
                                    exportOptions: {
                                    columns: ':visible:not(:eq(0))',
                                    rows:'.selected'
                                },
                                "className":'btn'
                                },
                                {
                                extend: 'excel',
                                text:'EXCEL<i class="material-icons right">file_download</i>',
                                exportOptions: {
                                rows:'.selected',
                                columns: ':visible:not(:eq(0))'
                                },
                                "className":'btn'
                                }
                                <?php
                                insertButton($Rol,"CampanasView");
                                ?>      //'colvis'         
                            ],
                    initComplete: function () {
                        tableCampanas.columns().every( function () {
                        var that = this;

                        //Searchers 
                        $( 'input', this.footer() ).change( function(event) {
                        if ( that.search() !== this.value ) {
                                that
                                .search( this.value )
                                .draw();

                    }                         
                    } );
                    });
                    },

                    footerCallback: function (row, data, start, end, display) {
                        var api=this.api(), data;

                        api.columns('.sum', {page: 'current'}).every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                        return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(1) td:eq(' + index + ')', tableCampanas.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                        api.columns('.sum').every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                            return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(2) td:eq(' + index + ')', tableCampanas.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                    }
                    });

    $('#' + startTable + ' tbody').on( 'click', 'tr', function () {
      $(this).toggleClass('selected');
    } );

    <?php
    $name="CampanasView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges2($name, $columnNames, $types, $Rol);
    ?>

//CLIENTESCAMPANAS


        startTable="ClientesCampanasView";

        <?php
    $name="ClientesCampanasView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges1($name, $columnNames, $types, $Rol);
    ?>

        var tableClientesCampanas = $('#' + startTable).DataTable({
                    scrollX:true,
                    autoWidth : true,
                    "language": {
                        "sUrl": "Language/Spanish.json"
                    },
                    dom: 'Blfrtip',
                    lengthMenu: [
                    [ 25, 50, -1 ],
                    [ '25', '50', 'Todos' ]
                    ],
                    buttons: [
                                {
                                    text: 'Filtros',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('columnSearcher');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            hide = 0;                                           
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                columnSearcherCleaner("ClientesCampanasView");
                                        }
                                        else 
                                        {
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-filtros'
                                },
                                {
                                    text: 'Rangos',
                                    action: function (e, dt, node, config) {
                                        var mins = document.getElementsByClassName('minimum');
                                        var maxs = document.getElementsByClassName('maximum');
                                        for(var i=0;i<mins.length;i++) {
                                            if(mins[i].style.display =='none') {
                                            mins[i].setAttribute('style', '');
                                            maxs[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            mins[i].style.display='none';
                                            maxs[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                minCleaner();
                                                maxCleaner();
                                        }
                                        else 
                                        {
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-rangos'
                                },
                                {
                                    text: 'Parciales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('partials');
                                        var raws = document.getElementsByClassName('emptyPartials');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-parciales'
                                },
                                {
                                    text: 'Totales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('totals');
                                        var raws = document.getElementsByClassName('emptyTotals');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-totales'

                                },

                                {
                                    extend: 'selectAll',
                                    text:'Todos<i class="material-icons right">done_all</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'selectNone',
                                    text:'Ninguno<i class="material-icons right">remove_circle_outline</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'csv',
                                    text:'CSV<i class="material-icons right">file_download</i>',
                                    exportOptions: {
                                    columns: ':visible:not(:eq(0))',
                                    rows:'.selected'
                                },
                                "className":'btn'
                                },
                                {
                                extend: 'excel',
                                text:'EXCEL<i class="material-icons right">file_download</i>',
                                exportOptions: {
                                rows:'.selected',
                                columns: ':visible:not(:eq(0))'
                                },
                                "className":'btn'
                                }
                                //NEW BUTTON NUEVO REGISTRO
                                <?php
                                insertButton($Rol,"ClientesCampanasView");
                                ?>     //'colvis'         
                            ],
                    initComplete: function () {
                        tableClientesCampanas.columns().every( function () {
                        var that = this;

                        //Searchers 
                        $( 'input', this.footer() ).change( function(event) {
                        if ( that.search() !== this.value ) {
                                that
                                .search( this.value )
                                .draw();
                    }                         
                    } );
                    });
                    },

                    footerCallback: function (row, data, start, end, display) {
                        var api=this.api(), data;

                        api.columns('.sum', {page: 'current'}).every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                        return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(1) td:eq(' + index + ')', tableClientesCampanas.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                        api.columns('.sum').every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                            return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(2) td:eq(' + index + ')', tableClientesCampanas.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                    }
                    });

    $('#' + startTable + ' tbody').on( 'click', 'tr', function () {
      $(this).toggleClass('selected');
    } );

    <?php
    $name="ClientesCampanasView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges2($name, $columnNames, $types, $Rol);
    ?>

    var startTable="ContactosView";

    <?php
    $name="ContactosView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges1($name, $columnNames, $types, $Rol);
    ?>

        var tableContactos = $('#' + startTable).DataTable({
                    scrollX:true,
                    autoWidth : true,
                    "language": {
                        "sUrl": "Language/Spanish.json"
                    },
                    dom: 'Blfrtip',
                    lengthMenu: [
                    [ 25, 50, -1 ],
                    [ '25', '50', 'Todos' ]
                    ],
                    buttons: [
                                {
                                    text: 'Filtros',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('columnSearcher');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            hide = 0;                                           
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                columnSearcherCleaner("ContactosView");
                                        }
                                        else 
                                        {
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-filtros'
                                },
                                {
                                    text: 'Rangos',
                                    action: function (e, dt, node, config) {
                                        var mins = document.getElementsByClassName('minimum');
                                        var maxs = document.getElementsByClassName('maximum');
                                        for(var i=0;i<mins.length;i++) {
                                            if(mins[i].style.display =='none') {
                                            mins[i].setAttribute('style', '');
                                            maxs[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            mins[i].style.display='none';
                                            maxs[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                       /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                minCleaner();
                                                maxCleaner();
                                        }
                                        else 
                                        {
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-rangos'
                                },
                                {
                                    text: 'Parciales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('partials');
                                        var raws = document.getElementsByClassName('emptyPartials');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-parciales'
                                },
                                {
                                    text: 'Totales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('totals');
                                        var raws = document.getElementsByClassName('emptyTotals');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-totales'
                                },

                                {
                                    extend: 'selectAll',
                                    text:'Todos<i class="material-icons right">done_all</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'selectNone',
                                    text:'Ninguno<i class="material-icons right">remove_circle_outline</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'csv',
                                    text:'CSV<i class="material-icons right">file_download</i>',
                                    exportOptions: {
                                    columns: ':visible:not(:eq(0))',
                                    rows:'.selected'
                                },
                                "className":'btn'
                                },
                                {
                                extend: 'excel',
                                text:'EXCEL<i class="material-icons right">file_download</i>',
                                exportOptions: {
                                rows:'.selected',
                                columns: ':visible:not(:eq(0))'
                                },
                                "className":'btn'
                                }
                                //NEW BUTTON NUEVO REGISTRO
                                <?php
                                insertButton($Rol,"ContactosView");
                                ?>
       //'colvis'         
                            ],
                    initComplete: function () {
                        tableContactos.columns().every( function () {
                        var that = this;

                        //Searchers 
                        $( 'input', this.footer() ).change( function(event) {
                        if ( that.search() !== this.value ) {
                                that
                                .search( this.value )
                                .draw();
                    }                         
                    } );
                    });
                    },

                    footerCallback: function (row, data, start, end, display) {
                        var api=this.api(), data;

                        api.columns('.sum', {page: 'current'}).every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                        return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(1) td:eq(' + index + ')', tableContactos.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                        api.columns('.sum').every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                            return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(2) td:eq(' + index + ')', tableContactos.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                    }
                    });

    $('#' + startTable + ' tbody').on( 'click', 'tr', function () {
      $(this).toggleClass('selected');
    } );

    <?php
    $name="ContactosView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges2($name, $columnNames, $types, $Rol);
    ?>

// FACTURAS


        var startTable="FacturasView";

        <?php
    $name="FacturasView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges1($name, $columnNames, $types, $Rol);
    ?>

        var tableFacturas = $('#' + startTable).DataTable({
                    scrollX:true,
                    autoWidth : true,
                    "language": {
                        "sUrl": "Language/Spanish.json"
                    },
                    dom: 'Blfrtip',
                    lengthMenu: [
                    [ 25, 50, -1 ],
                    [ '25', '50', 'Todos' ]
                    ],
                    buttons: [
                                {
                                    text: 'Filtros',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('columnSearcher');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', ''); 
                                            hide = 0;                                          
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                columnSearcherCleaner("FacturasView");
                                        }
                                        else 
                                        {
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-filtros'
                                },
                                {
                                    text: 'Rangos',
                                    action: function (e, dt, node, config) {
                                        var mins = document.getElementsByClassName('minimum');
                                        var maxs = document.getElementsByClassName('maximum');
                                        for(var i=0;i<mins.length;i++) {
                                            if(mins[i].style.display =='none') {
                                            mins[i].setAttribute('style', '');
                                            maxs[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            mins[i].style.display='none';
                                            maxs[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                minCleaner();
                                                maxCleaner();
                                        }
                                        else 
                                        {
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-rangos'
                                },
                                {
                                    text: 'Parciales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('partials');
                                        var raws = document.getElementsByClassName('emptyPartials');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-parciales'
                                },
                                {
                                    text: 'Totales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('totals');
                                        var raws = document.getElementsByClassName('emptyTotals');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-totales'
                                },

                                {
                                    extend: 'selectAll',
                                    text:'Todos<i class="material-icons right">done_all</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'selectNone',
                                    text:'Ninguno<i class="material-icons right">remove_circle_outline</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'csv',
                                    text:'CSV<i class="material-icons right">file_download</i>',
                                    exportOptions: {
                                    columns: ':visible',
                                    rows:'.selected'
                                },
                                "className":'btn'
                                },
                                {
                                extend: 'excel',
                                text:'EXCEL<i class="material-icons right">file_download</i>',
                                exportOptions: {
                                rows:'.selected',
                                columns: ':visible'
                                },
                                "className":'btn'
                                }       //'colvis'         
                            ],
                    initComplete: function () {
                        tableFacturas.columns().every( function () {
                        var that = this;

                        //Searchers 
                        $( 'input', this.footer() ).change( function(event) {
                        if ( that.search() !== this.value ) {
                                that
                                .search( this.value )
                                .draw();
                    }                         
                    } );
                    });
                    },

                    footerCallback: function (row, data, start, end, display) {
                        var api=this.api(), data;

                        api.columns('.sum', {page: 'current'}).every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                        return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(1) td:eq(' + index + ')', tableFacturas.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                        api.columns('.sum').every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                            return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(2) td:eq(' + index + ')', tableFacturas.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                    }
                    });

    $('#' + startTable + ' tbody').on( 'click', 'tr', function () {
      $(this).toggleClass('selected');
    } );

    <?php
    $name="FacturasView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges2($name, $columnNames, $types, $Rol);
    ?>

    startTable="GruposView";

    <?php
    $name="GruposView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges1($name, $columnNames, $types, $Rol);
    ?>

        var tableGrupos = $('#' + startTable).DataTable({
                    scrollX:true,
                    autoWidth : true,
                    "language": {
                        "sUrl": "Language/Spanish.json"
                    },
                    dom: 'Blfrtip',
                    lengthMenu: [
                    [ 25, 50, -1 ],
                    [ '25', '50', 'Todos' ]
                    ],
                    buttons: [
                                {
                                    text: 'Filtros',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('columnSearcher');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            hide = 0;                                           
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                columnSearcherCleaner("GruposView");
                                        }
                                        else 
                                        {
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-filtros'
                                },
                                {
                                    text: 'Rangos',
                                    action: function (e, dt, node, config) {
                                        var mins = document.getElementsByClassName('minimum');
                                        var maxs = document.getElementsByClassName('maximum');
                                        for(var i=0;i<mins.length;i++) {
                                            if(mins[i].style.display =='none') {
                                            mins[i].setAttribute('style', '');
                                            maxs[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            mins[i].style.display='none';
                                            maxs[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                minCleaner();
                                                maxCleaner();
                                        }
                                        else 
                                        {
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-rangos'
                                },
                                {
                                    text: 'Parciales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('partials');
                                        var raws = document.getElementsByClassName('emptyPartials');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-parciales'
                                },
                                {
                                    text: 'Totales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('totals');
                                        var raws = document.getElementsByClassName('emptyTotals');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-totales'
                                },

                                {
                                    extend: 'selectAll',
                                    text:'Todos<i class="material-icons right">done_all</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'selectNone',
                                    text:'Ninguno<i class="material-icons right">remove_circle_outline</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'csv',
                                    text:'CSV<i class="material-icons right">file_download</i>',
                                    exportOptions: {
                                    columns: ':visible',
                                    rows:'.selected'
                                },
                                "className":'btn'
                                },
                                {
                                extend: 'excel',
                                text:'EXCEL<i class="material-icons right">file_download</i>',
                                exportOptions: {
                                rows:'.selected',
                                columns: ':visible'
                                },
                                "className":'btn'
                                }      //'colvis'         
                            ],
                    initComplete: function () {
                        tableGrupos.columns().every( function () {
                        var that = this;

                        //Searchers 
                        $( 'input', this.footer() ).change( function(event) {
                        if ( that.search() !== this.value ) {
                                that
                                .search( this.value )
                                .draw();
                            }                       
                    } );
                    });
                    },

                    footerCallback: function (row, data, start, end, display) {
                        var api=this.api(), data;

                        api.columns('.sum', {page: 'current'}).every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                        return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(1) td:eq(' + index + ')', tableGrupos.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                        api.columns('.sum').every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                            return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(2) td:eq(' + index + ')', tableGrupos.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                    }
                    });

    $('#' + startTable + ' tbody').on( 'click', 'tr', function () {
      $(this).toggleClass('selected');
    } );

    <?php
    $name="GruposView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges2($name, $columnNames, $types, $Rol);
    ?>



        startTable="MembresiasView";

        <?php
    $name="MembresiasView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges1($name, $columnNames, $types, $Rol);
    ?>

        var tableMembresias = $('#' + startTable).DataTable({
                    scrollX:true,
                    autoWidth : true,
                    "language": {
                        "sUrl": "Language/Spanish.json"
                    },
                    dom: 'Blfrtip',
                    lengthMenu: [
                    [ 25, 50, -1 ],
                    [ '25', '50', 'Todos' ]
                    ],
                    buttons: [
                                {
                                    text: 'Filtros',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('columnSearcher');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            hide = 0;                                           
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                columnSearcherCleaner("MembresiasView");
                                        }
                                        else 
                                        {
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-filtros'
                                },
                                {
                                    text: 'Rangos',
                                    action: function (e, dt, node, config) {
                                        var mins = document.getElementsByClassName('minimum');
                                        var maxs = document.getElementsByClassName('maximum');
                                        for(var i=0;i<mins.length;i++) {
                                            if(mins[i].style.display =='none') {
                                            mins[i].setAttribute('style', '');
                                            maxs[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            mins[i].style.display='none';
                                            maxs[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                minCleaner();
                                                maxCleaner();
                                        }
                                        else 
                                        {
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-rangos'
                                },
                                {
                                    text: 'Parciales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('partials');
                                        var raws = document.getElementsByClassName('emptyPartials');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-parciales'
                                },
                                {
                                    text: 'Totales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('totals');
                                        var raws = document.getElementsByClassName('emptyTotals');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-totales'
                                },

                                {
                                    extend: 'selectAll',
                                    text:'Todos<i class="material-icons right">done_all</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'selectNone',
                                    text:'Ninguno<i class="material-icons right">remove_circle_outline</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'csv',
                                    text:'CSV<i class="material-icons right">file_download</i>',
                                    exportOptions: {
                                    columns: ':visible',
                                    rows:'.selected'
                                },
                                "className":'btn'
                                },
                                {
                                extend: 'excel',
                                text:'EXCEL<i class="material-icons right">file_download</i>',
                                exportOptions: {
                                rows:'.selected',
                                columns: ':visible'
                                },
                                "className":'btn'
                                }      //'colvis'         
                            ],
                    initComplete: function () {
                        tableMembresias.columns().every( function () {
                        var that = this;

                        //Searchers 
                        $( 'input', this.footer() ).change( function(event) {
                        if ( that.search() !== this.value ) {
                                that
                                .search( this.value )
                                .draw();
                            }                      
                    } );
                    });
                    },

                    footerCallback: function (row, data, start, end, display) {
                        var api=this.api(), data;

                        api.columns('.sum', {page: 'current'}).every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                        return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(1) td:eq(' + index + ')', tableMembresias.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                        api.columns('.sum').every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                            return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(2) td:eq(' + index + ')', tableMembresias.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                    }
                    });

    $('#' + startTable + ' tbody').on( 'click', 'tr', function () {
      $(this).toggleClass('selected');
    } );

    <?php
    $name="MembresiasView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges2($name, $columnNames, $types, $Rol);
    ?>

    startTable="OportunidadesView";

    <?php
    $name="OportunidadesView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges1($name, $columnNames, $types, $Rol);
    ?>

        var tableOportunidades = $('#' + startTable).DataTable({
                    scrollX:true,
                    autoWidth : true,
                    "language": {
                        "sUrl": "Language/Spanish.json"
                    },
                    dom: 'Blfrtip',
                    lengthMenu: [
                    [ 25, 50, -1 ],
                    [ '25', '50', 'Todos' ]
                    ],
                    buttons: [
                                {
                                    text: 'Filtros',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('columnSearcher');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            hide = 0;                                           
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                columnSearcherCleaner("OportunidadesView");
                                        }
                                        else 
                                        {
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-filtros'
                                },
                                {
                                    text: 'Rangos',
                                    action: function (e, dt, node, config) {
                                        var mins = document.getElementsByClassName('minimum');
                                        var maxs = document.getElementsByClassName('maximum');
                                        for(var i=0;i<mins.length;i++) {
                                            if(mins[i].style.display =='none') {
                                            mins[i].setAttribute('style', '');
                                            maxs[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            mins[i].style.display='none';
                                            maxs[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                minCleaner();
                                                maxCleaner();
                                        }
                                        else 
                                        {
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-rangos'
                                },
                                {
                                    text: 'Parciales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('partials');
                                        var raws = document.getElementsByClassName('emptyPartials');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-parciales'
                                },
                                {
                                    text: 'Totales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('totals');
                                        var raws = document.getElementsByClassName('emptyTotals');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-totales'
                                },

                                {
                                    extend: 'selectAll',
                                    text:'Todos<i class="material-icons right">done_all</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'selectNone',
                                    text:'Ninguno<i class="material-icons right">remove_circle_outline</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'csv',
                                    text:'CSV<i class="material-icons right">file_download</i>',
                                    exportOptions: {
                                    columns: ':visible',
                                    rows:'.selected'
                                },
                                "className":'btn'
                                },
                                {
                                extend: 'excel',
                                text:'EXCEL<i class="material-icons right">file_download</i>',
                                exportOptions: {
                                rows:'.selected',
                                columns: ':visible'
                                },
                                "className":'btn'
                                }
                                <?php
                                insertButton($Rol,"OportunidadesView");
                                ?>     //'colvis'         
                            ],
                    initComplete: function () {
                        tableOportunidades.columns().every( function () {
                        var that = this;

                        //Searchers 
                        $( 'input', this.footer() ).change( function(event) {
                        if ( that.search() !== this.value ) {
                                that
                                .search( this.value )
                                .draw();
                            }                        
                    } );
                    });
                    },

                    footerCallback: function (row, data, start, end, display) {
                        var api=this.api(), data;

                        api.columns('.sum', {page: 'current'}).every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                        return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(1) td:eq(' + index + ')', tableOportunidades.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                        api.columns('.sum').every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                            return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(2) td:eq(' + index + ')', tableOportunidades.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                    }
                    });

    $('#' + startTable + ' tbody').on( 'click', 'tr', function () {
      $(this).toggleClass('selected');
    } );

    <?php
    $name="OportunidadesView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges2($name, $columnNames, $types, $Rol);
    ?>

    startTable="RolesView";

    <?php
    $name="RolesView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges1($name, $columnNames, $types, $Rol);
    ?>

        var tableRoles = $('#' + startTable).DataTable({
                    scrollX:true,
                    autoWidth : true,
                    "language": {
                        "sUrl": "Language/Spanish.json"
                    },
                    dom: 'Blfrtip',
                    lengthMenu: [
                    [ 25, 50, -1 ],
                    [ '25', '50', 'Todos' ]
                    ],
                    buttons: [
                                {
                                    text: 'Filtros',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('columnSearcher');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', ''); 
                                            hide = 0;                                          
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                columnSearcherCleaner("RolesView");
                                        }
                                        else 
                                        {
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-filtros'
                                },
                                {
                                    text: 'Rangos',
                                    action: function (e, dt, node, config) {
                                        var mins = document.getElementsByClassName('minimum');
                                        var maxs = document.getElementsByClassName('maximum');
                                        for(var i=0;i<mins.length;i++) {
                                            if(mins[i].style.display =='none') {
                                            mins[i].setAttribute('style', '');
                                            maxs[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            mins[i].style.display='none';
                                            maxs[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                minCleaner();
                                                maxCleaner();
                                        }
                                        else 
                                        {
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-rangos'
                                },
                                {
                                    text: 'Parciales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('partials');
                                        var raws = document.getElementsByClassName('emptyPartials');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-parciales'
                                },
                                {
                                    text: 'Totales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('totals');
                                        var raws = document.getElementsByClassName('emptyTotals');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-totales'
                                },

                                {
                                    extend: 'selectAll',
                                    text:'Todos<i class="material-icons right">done_all</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'selectNone',
                                    text:'Ninguno<i class="material-icons right">remove_circle_outline</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'csv',
                                    text:'CSV<i class="material-icons right">file_download</i>',
                                    exportOptions: {
                                    columns: ':visible',
                                    rows:'.selected'
                                },
                                "className":'btn'
                                },
                                {
                                extend: 'excel',
                                text:'EXCEL<i class="material-icons right">file_download</i>',
                                exportOptions: {
                                rows:'.selected',
                                columns: ':visible'
                                },
                                "className":'btn'
                                }      //'colvis'         
                            ],
                    initComplete: function () {
                        tableRoles.columns().every( function () {
                        var that = this;

                        //Searchers 
                        $( 'input', this.footer() ).change( function(event) {
                        if ( that.search() !== this.value ) {
                                that
                                .search( this.value )
                                .draw();
                            }                        
                    } );
                    });
                    },

                    footerCallback: function (row, data, start, end, display) {
                        var api=this.api(), data;

                        api.columns('.sum', {page: 'current'}).every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                        return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(1) td:eq(' + index + ')', tableRoles.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                        api.columns('.sum').every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                            return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(2) td:eq(' + index + ')', tableRoles.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                    }
                    });

    $('#' + startTable + ' tbody').on( 'click', 'tr', function () {
      $(this).toggleClass('selected');
    } );

    <?php
    $name="RolesView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges2($name, $columnNames, $types, $Rol);
    ?>



        startTable="UsuariosView";

        <?php
    $name="UsuariosView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges1($name, $columnNames, $types, $Rol);
    ?>

        var tableUsuarios = $('#' + startTable).DataTable({
                    scrollX:true,
                    autoWidth : true,
                    "language": {
                        "sUrl": "Language/Spanish.json"
                    },
                    dom: 'Blfrtip',
                    lengthMenu: [
                    [ 25, 50, -1 ],
                    [ '25', '50', 'Todos' ]
                    ],
                    buttons: [
                                {
                                    text: 'Filtros',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('columnSearcher');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');  
                                            hide = 0;                                         
                                        }
                                                else {
                                                    rows[i].style.display='none';
                                                    hide = 1;
                                                }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                columnSearcherCleaner("UsuariosView");
                                        }
                                        else 
                                        {
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-filtros'
                                },
                                {
                                    text: 'Rangos',
                                    action: function (e, dt, node, config) {
                                        var mins = document.getElementsByClassName('minimum');
                                        var maxs = document.getElementsByClassName('maximum');
                                        for(var i=0;i<mins.length;i++) {
                                            if(mins[i].style.display =='none') {
                                            mins[i].setAttribute('style', '');
                                            maxs[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            mins[i].style.display='none';
                                            maxs[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                minCleaner();
                                                maxCleaner();
                                        }
                                        else 
                                        {
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-rangos'
                                },
                                {
                                    text: 'Parciales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('partials');
                                        var raws = document.getElementsByClassName('emptyPartials');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', ''); 
                                            hide = 0;                                           
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-parciales'
                                },
                                {
                                    text: 'Totales',
                                    action: function (e, dt, node, config) {
                                        var rows = document.getElementsByClassName('totals');
                                        var raws = document.getElementsByClassName('emptyTotals');
                                        for(var i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide = 0;
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-totales'
                                },

                                {
                                    extend: 'selectAll',
                                    text:'Todos<i class="material-icons right">done_all</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'selectNone',
                                    text:'Ninguno<i class="material-icons right">remove_circle_outline</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'csv',
                                    text:'CSV<i class="material-icons right">file_download</i>',
                                    exportOptions: {
                                    columns: ':visible',
                                    rows:'.selected'
                                },
                                "className":'btn'
                                },
                                {
                                extend: 'excel',
                                text:'EXCEL<i class="material-icons right">file_download</i>',
                                exportOptions: {
                                rows:'.selected',
                                columns: ':visible'
                                },
                                "className":'btn'
                                }      //'colvis'         
                            ],
                    initComplete: function () {
                        tableUsuarios.columns().every( function () {
                        var that = this;

                        //Searchers 
                        $( 'input', this.footer() ).change( function(event) {
                        if ( that.search() !== this.value ) {
                                that
                                .search( this.value )
                                .draw();
                            }          
                    } );
                    });

                    },

                    footerCallback: function (row, data, start, end, display) {
                        var api=this.api(), data;

                        api.columns('.sum', {page: 'current'}).every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                        return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(1) td:eq(' + index + ')', tableUsuarios.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                        api.columns('.sum').every(function (index) {
                        var sum = this
                        .data()
                        .reduce( function (a,b) {
                            return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                        var num = index;
                        var cell = $('tr:eq(2) td:eq(' + index + ')', tableUsuarios.table().footer());
                        cell.html(parseFloat(sum).toFixed(2));
                        });

                    }
                    });

    $('#' + startTable + ' tbody').on( 'click', 'tr', function () {
      $(this).toggleClass('selected');
    } );

    <?php
    $name="UsuariosView";
        $columnNames=getColumnNames($name);
        $types=getAllTypes($name);
        initialiseRanges2($name, $columnNames, $types, $Rol);
    ?>


});




            function displaytable(table, allviews) 
            {
                var views = allviews.split(",");

                for(var i=0;i<views.length;i++) 
                {
                    var x = document.getElementById("div_" + views[i].trim());
                    x.style.display = "none";
                }
                var y = document.getElementById("div_" + table.trim());
                y.style.display = 'block';

                //$('#table.trim').DataTable().fnSettings().oScroller.fnMeasure();
                $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;

            }

            function setInnerText(obj, text) {
                var object = document.getElementById(obj);
                object.innerText = text;
            }

            function columnSearcherCleaner(View) {
            $('.Filter' + View).each( function () {
                if($(this).find('input').val().length) 
                {
                    $(this).find('input').attr("placeholder", "Buscar");
                    $(this).find('input').val('');
                    $(this).find('input').change();
                }
                                                                          
            });
        }

        function minCleaner() {
            $('.minimum').each( function () {
                if($(this).find('input').val()) {
                    $(this).find('input').attr("placeholder", "Min");
                    $(this).find('input').val('');
                    $(this).find('input').change();
                }
            });
        }

        function maxCleaner() {
            $('.maximum').each( function () {
                if($(this).find('input').val()) {
                    $(this).find('input').attr("placeholder", "Max");
                    $(this).find('input').val('');
                    $(this).find('input').change();
                }
            });
        }

        function changeLeftBarColours(btn) {
            $('.leftBarbtn').each(function() {
                $(this).setAttribute("background-color", "red");
                });
            $('#' + this).setAttribute("background-color", "blue");
        }

        </script>
       <style>
		
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="materialize/js/materialize.min.js"></script>
    </style>
</html>
